import os
from collections import defaultdict

from config import Config
from .extract import extract_data
from .transform import get_testcases
from .load import load


def create_out_dirs():
    for directory in Config.out.values():
        if not os.path.isdir(directory):
            os.makedirs(directory)


def main():
    print('Criando diretorios...')
    create_out_dirs()
    data_dicts = defaultdict()
    for data_type in ['platform', 'testsuite']:
        print(f'Extraindo dados de {data_type}...')
        data_dicts[data_type] = extract_data(data_type)

    data_dicts['testcase'] = defaultdict()
    for suite_file, suite in data_dicts['testsuite'].items():
        print(f'Obtendo dados de {suite_file}...')
        data_dicts['testcase'][suite_file] = get_testcases(suite)

    print(f'Salvando arquivos...')
    load(data_dicts)


if __name__ == '__main__':
    main()
