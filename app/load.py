import os
import re
import json

from config import Config


def save_dict_to_json(input_dict, json_output):
    print(f'Salvando {json_output}...')
    with open(json_output, 'w', encoding='utf-8-sig') as output_file:
        json.dump(input_dict, output_file, indent=4, ensure_ascii=False)


def strip_chars(text):
    return re.sub('[ /<>]', '', text)


def load_data(data, data_type, outpath):
    if data_type == 'testcase':
        for basefile, suite in data.items():
            for suite_name, testcases in suite.items():
                outfile = strip_chars(
                    f'{os.path.splitext(basefile)[0]}_{suite_name}.json'
                )
                outpath = os.path.join(Config.out[data_type], outfile)
                save_dict_to_json(testcases, outpath)
    else:
        for srcfile, content in data.items():
            outfile = strip_chars(
                f'{os.path.splitext(srcfile)[0]}.json'
            )
            outpath = os.path.join(Config.out[data_type], outfile)
            save_dict_to_json(content, outpath)


def load(final_dicts):
    for data_type, outpath in Config.out.items():
        if data_type in final_dicts.keys():
            load_data(final_dicts[data_type], data_type, outpath)
