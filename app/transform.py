from collections import defaultdict


def get_step(step):
    if step:
        return {
            'action': step.get('actions'),
            'data': '',
            'result': step.get('expectedresults')
        }
    else:
        return {}


def transform_suite(testcases):
    ret = []
    for testcase in testcases:
        if len(testcase) > 0:
            pretty_tc = defaultdict()
            pretty_tc['fields'] = {
                'summary': testcase.get('@name'),
                'priority': {'name': 'Medium'},
                'components': [],
                'project': {'key': 'TM001'}
            }
            pretty_tc['testtype'] = 'Manual'
            pretty_tc['steps'] = []
            steps = testcase.get('steps', {}).get('step')
            if type(steps) == list:
                for step in steps:
                    pretty_tc['steps'].append(get_step(step))
            else:
                pretty_tc['steps'].append(get_step(steps))
            ret.append(pretty_tc)

    return ret


def get_testcases(testsuite):
    ret = defaultdict()
    for suite_name, testcases in testsuite.items():
        ret[suite_name] = transform_suite(testcases)

    return ret
