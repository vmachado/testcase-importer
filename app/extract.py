import os
from collections import defaultdict

import xmltodict

from config import Config


def clean_platform_data(raw_dict) -> dict:
    return raw_dict.get('platforms')


def clean_testsuite_data(raw_dict) -> dict:
    return flatten_suites(raw_dict)


CLEAN_OPTIONS = {
    'platform': clean_platform_data,
    'testsuite': clean_testsuite_data
}


def read_xml_file(xml_file):
    with open(xml_file, encoding='utf-8') as input_file:
        xml = input_file.read()

    return xml


def convert_xml_to_dict(xml_file):
    xml = read_xml_file(xml_file)
    return xmltodict.parse(xml)


def flatten_suites(suite, parent_name=''):
    """Extract all test suites and include them in a single-level dict.
    Structure of the final dict:
    {
        'test_suite1': ['test_cases'],
        'test_suite1.1': ['test_cases'],
        'test_suite2': ['test_cases'],
        ...
    }"""
    ret = defaultdict()
    subsuites = suite.get('testsuite')
    testcases = suite.get('testcase', {})
    suite_name = suite.get('@name')
    full_name = f'{parent_name}_{suite_name}' if parent_name else suite_name
    if subsuites is not None:
        if type(subsuites) is list:
            for subsuite in subsuites:
                ret.update(flatten_suites(subsuite, full_name))
        else:
            ret.update(flatten_suites(subsuites, full_name))

    if suite_name not in ['', None]:
        ret[full_name] = testcases if type(testcases) is list else [testcases]

    return(ret)


def extract_data(data_type):
    ret = defaultdict()
    for src_file in next(os.walk(Config.data[data_type]))[2]:
        src_path = os.path.join(Config.data[data_type], src_file)
        converted_dict = convert_xml_to_dict(src_path)
        ret.update(
            {
                src_file: CLEAN_OPTIONS[data_type](converted_dict)
            }
        )

    return ret
