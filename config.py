import os


class Config:
    data = {
        'root': os.path.join(os.getcwd(), 'in')
    }
    data['testsuite'] = os.path.join(data['root'], 'posweb_full')
    data['platform'] = os.path.join(data['root'], 'platforms')
    data['testcase'] = os.path.join(data['root'], 'testcases')

    out = {
        'root': os.path.join(os.getcwd(), 'out')
    }
    out['testsuite'] = os.path.join(out['root'], 'testsuites')
    out['platform'] = os.path.join(out['root'], 'platforms')
    out['testcase'] = os.path.join(out['root'], 'testcases')
