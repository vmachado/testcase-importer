import argparse

from app import main as app


def parse_arguments() -> argparse.ArgumentParser:
    """Return the application arguments."""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--tojson', dest='tojson',
        help='Diretorio ou arquivo para conversao de arquivos .xml para json.',
        required=True
    )

    return parser


if __name__ == '__main__':
    app()
